import 'package:shared_preferences/shared_preferences.dart';
import 'dart:io';
import 'dart:convert';

class UserSimplePreferences {
  static late SharedPreferences _preferences;

  static Future init() async =>
      _preferences = await SharedPreferences.getInstance();

  static Future setTeam(int teamNumber) async {
    if (_preferences.getString("Team$teamNumber") == null) {
      String apiUrl =
          'https://v3.football.api-sports.io/players/squads?team=$teamNumber';
      HttpClient client = HttpClient();
      client.autoUncompress = true;
      final HttpClientRequest request = await client.getUrl(Uri.parse(apiUrl));
      request.headers
          .set(HttpHeaders.contentTypeHeader, "application/json;charset=UTF-8");
      request.headers.set("X-Rapidapi-Key", "db6802318ba45cb867485a165d382e1e");
      final HttpClientResponse response = await request.close();
      final String data = await response.transform(utf8.decoder).join();
      // print(data);
      // print("Team $teamNumber");
      await _preferences.setString("Team$teamNumber", data);
    }
  }

  static Future setCurrentState(int teamNumber) async {
    await _preferences.setInt("pageIndex", teamNumber);
  }

  static Future setStats(int teamNumber) async {
    if (_preferences.getString("Stats$teamNumber") == null) {
      String apiUrl =
          'https://v3.football.api-sports.io/teams/statistics?league=39&season=2019&team=$teamNumber';
      HttpClient client = HttpClient();
      client.autoUncompress = true;
      final HttpClientRequest request = await client.getUrl(Uri.parse(apiUrl));
      request.headers
          .set(HttpHeaders.contentTypeHeader, "application/json;charset=UTF-8");
      request.headers.set("X-Rapidapi-Key", "db6802318ba45cb867485a165d382e1e");
      final HttpClientResponse response = await request.close();
      final String data = await response.transform(utf8.decoder).join();
      // print(data);
      // print("Stats $teamNumber");
      await _preferences.setString("Stats$teamNumber", data);
    }
  }

  static String? getTeam(int teamNumber) {
    return _preferences.getString("Team$teamNumber");
  }

  static String? getStats(int teamNumber) {
    return _preferences.getString("Stats$teamNumber");
  }

  static int? getCurrentState() {
    return _preferences.getInt("pageIndex");
  }
}
