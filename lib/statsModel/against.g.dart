// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'against.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Against _$AgainstFromJson(Map<String, dynamic> json) => Against(
      Breakdown.fromJson(json['total'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$AgainstToJson(Against instance) => <String, dynamic>{
      'total': instance.total.toJson(),
    };
