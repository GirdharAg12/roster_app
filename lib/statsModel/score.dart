import 'package:json_annotation/json_annotation.dart';
import 'for.dart';
import 'against.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'score.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.

@JsonSerializable(explicitToJson: true)
class Score {
  Score(this.fr, this.against);

  @JsonKey(name: 'for')
  For fr;

  @JsonKey(name: 'against')
  Against against;

  factory Score.fromJson(Map<String, dynamic> json) => _$ScoreFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$ScoreToJson(this);
}
