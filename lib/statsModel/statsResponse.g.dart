// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'statsResponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

StatsResponse _$StatsResponseFromJson(Map<String, dynamic> json) =>
    StatsResponse(
      Matches.fromJson(json['fixtures'] as Map<String, dynamic>),
      Score.fromJson(json['goals'] as Map<String, dynamic>),
      Breakdown.fromJson(json['clean_sheet'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$StatsResponseToJson(StatsResponse instance) =>
    <String, dynamic>{
      'fixtures': instance.matches.toJson(),
      'goals': instance.score.toJson(),
      'clean_sheet': instance.cleansheet.toJson(),
    };
