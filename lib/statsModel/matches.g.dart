// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'matches.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Matches _$MatchesFromJson(Map<String, dynamic> json) => Matches(
      Breakdown.fromJson(json['played'] as Map<String, dynamic>),
      Breakdown.fromJson(json['wins'] as Map<String, dynamic>),
      Breakdown.fromJson(json['draws'] as Map<String, dynamic>),
      Breakdown.fromJson(json['loses'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$MatchesToJson(Matches instance) => <String, dynamic>{
      'played': instance.played.toJson(),
      'wins': instance.wins.toJson(),
      'draws': instance.draws.toJson(),
      'loses': instance.losses.toJson(),
    };
