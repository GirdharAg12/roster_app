import 'package:json_annotation/json_annotation.dart';
import 'matches.dart';
import 'score.dart';
import 'breakdown.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'statsResponse.g.dart';

@JsonSerializable(explicitToJson: true)
class StatsResponse {
  StatsResponse(this.matches, this.score, this.cleansheet);

  @JsonKey(name: 'fixtures')
  Matches matches;

  @JsonKey(name: 'goals')
  Score score;

  @JsonKey(name: 'clean_sheet')
  Breakdown cleansheet;

  factory StatsResponse.fromJson(Map<String, dynamic> json) =>
      _$StatsResponseFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$StatsResponseToJson(this);
}
