import 'package:json_annotation/json_annotation.dart';
import 'breakdown.dart';

part 'for.g.dart';

@JsonSerializable(explicitToJson: true)
class For {
  For(this.total);

  @JsonKey(name: 'total')
  Breakdown total;

  factory For.fromJson(Map<String, dynamic> json) => _$ForFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$ForToJson(this);
}
