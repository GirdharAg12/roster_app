// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'score.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Score _$ScoreFromJson(Map<String, dynamic> json) => Score(
      For.fromJson(json['for'] as Map<String, dynamic>),
      Against.fromJson(json['against'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ScoreToJson(Score instance) => <String, dynamic>{
      'for': instance.fr.toJson(),
      'against': instance.against.toJson(),
    };
