// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'breakdown.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Breakdown _$BreakdownFromJson(Map<String, dynamic> json) => Breakdown(
      json['home'] as int? ?? 0,
      json['away'] as int? ?? 0,
      json['total'] as int? ?? 0,
    );

Map<String, dynamic> _$BreakdownToJson(Breakdown instance) => <String, dynamic>{
      'home': instance.home,
      'away': instance.away,
      'total': instance.total,
    };
