import 'package:json_annotation/json_annotation.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'breakdown.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.

@JsonSerializable(explicitToJson: true)
class Breakdown {
  Breakdown(this.home, this.away, this.total);

  @JsonKey(defaultValue: 0)
  int home;

  @JsonKey(defaultValue: 0)
  int away;

  @JsonKey(defaultValue: 0)
  int total;

  factory Breakdown.fromJson(Map<String, dynamic> json) =>
      _$BreakdownFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$BreakdownToJson(this);
}
