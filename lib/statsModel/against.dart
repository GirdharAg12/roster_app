import 'package:json_annotation/json_annotation.dart';
import 'breakdown.dart';

part 'against.g.dart';

@JsonSerializable(explicitToJson: true)
class Against {
  Against(this.total);

  @JsonKey(name: 'total')
  Breakdown total;

  factory Against.fromJson(Map<String, dynamic> json) =>
      _$AgainstFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$AgainstToJson(this);
}
