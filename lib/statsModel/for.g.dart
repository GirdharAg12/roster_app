// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'for.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

For _$ForFromJson(Map<String, dynamic> json) => For(
      Breakdown.fromJson(json['total'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ForToJson(For instance) => <String, dynamic>{
      'total': instance.total.toJson(),
    };
