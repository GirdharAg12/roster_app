import 'package:json_annotation/json_annotation.dart';
import 'breakdown.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'matches.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.

@JsonSerializable(explicitToJson: true)
class Matches {
  Matches(this.played, this.wins, this.draws, this.losses);

  @JsonKey(name: 'played')
  Breakdown played;

  @JsonKey(name: 'wins')
  Breakdown wins;

  @JsonKey(name: 'draws')
  Breakdown draws;

  @JsonKey(name: 'loses')
  Breakdown losses;

  factory Matches.fromJson(Map<String, dynamic> json) =>
      _$MatchesFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$MatchesToJson(this);
}
