// ignore_for_file: prefer_typing_uninitialized_variables

import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:rosterapp/size_config.dart';
import 'dart:io';
import 'dart:async';
import 'package:path_provider/path_provider.dart';
import 'package:rosterapp/squadModel/teamResponse.dart';
import 'package:rosterapp/statsModel/statsResponse.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // await UserSimplePreferences.init();
  runApp(const MyApp());
}

Widget statsHeading(String heading) {
  return Text(
    heading,
    style: const TextStyle(
      color: Color.fromARGB(255, 56, 0, 60),
      fontWeight: FontWeight.w500,
    ),
  );
}

Widget statsContent(int content) {
  return Text(
    "$content",
    style: const TextStyle(
      color: Color.fromARGB(255, 56, 0, 60),
      fontWeight: FontWeight.bold,
    ),
  );
}

Widget statsContainer(String heading, int content) {
  return Container(
    padding: EdgeInsets.fromLTRB(
        SizeConfig.blockSizeHorizontal! * 1,
        SizeConfig.blockSizeVertical! * 2,
        SizeConfig.blockSizeHorizontal! * 1,
        SizeConfig.blockSizeVertical! * 1.5),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        statsHeading(heading),
        statsContent(content),
      ],
    ),
  );
}

Widget stats() {
  return Container(
    margin: EdgeInsets.fromLTRB(0, SizeConfig.blockSizeVertical! * 5, 0, 0),
    width: SizeConfig.blockSizeHorizontal! * 93,
    height: SizeConfig.blockSizeVertical! * 4,
    child: Column(
      children: <Widget>[
        Container(
          color: const Color.fromARGB(255, 56, 0, 60),
          width: SizeConfig.blockSizeHorizontal! * 93,
          height: SizeConfig.blockSizeVertical! * 4,
          child: const Text(""),
        ),
        Table(
            defaultVerticalAlignment: TableCellVerticalAlignment.middle,
            columnWidths: {
              0: FixedColumnWidth(SizeConfig.blockSizeHorizontal! * 31),
              1: FixedColumnWidth(SizeConfig.blockSizeHorizontal! * 31),
              2: FixedColumnWidth(SizeConfig.blockSizeHorizontal! * 31)
            },
            border: TableBorder.all(color: Colors.black, width: 0.1),
            children: [
              TableRow(children: [
                statsContainer("Matches played", _stats!.matches.played.total),
                statsContainer("Wins", _stats!.matches.wins.total),
                statsContainer("Loses", _stats!.matches.losses.total)
              ]),
              TableRow(children: [
                statsContainer("Goals", _stats!.score.fr.total.total),
                statsContainer(
                    "Goals Conceded", _stats!.score.against.total.total),
                statsContainer("Clean sheets", _stats!.cleansheet.total),
              ]),
            ])
      ],
    ),
  );
}

Widget squad() {
  return ListView.separated(
    padding: const EdgeInsets.all(8),
    itemCount: _team!.players.length,
    physics: const BouncingScrollPhysics(),
    itemBuilder: (BuildContext context, int index) {
      return Container(
        height: SizeConfig.blockSizeVertical! * 13,
        padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal! * 3),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Image.network(
                _team!.players[index].pic,
                height: SizeConfig.blockSizeVertical! * 10,
              ),
              Text(
                _team!.players[index].number.toString(),
                // _team[0]['players'][index]['number'].toString(),
                style: TextStyle(
                    color: const Color.fromARGB(255, 56, 0, 60),
                    fontSize: SizeConfig.blockSizeVertical! * 2.5),
              ),
              Container(
                padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal! * 2),
                width: SizeConfig.blockSizeHorizontal! * 50,
                // color: Colors.red,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      _team!.players[index].name,
                      // _team[0]['players'][index]['name'],
                      style: const TextStyle(
                          color: Color.fromARGB(255, 56, 0, 60),
                          fontWeight: FontWeight.bold),
                    ),
                    Text(_team!.players[index].designation,
                        // _team[0]['players'][index]['position'],
                        style: const TextStyle(
                            color: Color.fromARGB(255, 56, 0, 60))),
                  ],
                ),
              )
            ]),
      );
    },
    separatorBuilder: (BuildContext context, int index) => const Divider(),
  );
}

Widget tabBar() {
  return TabBar(
    isScrollable: true,
    labelPadding: EdgeInsets.fromLTRB(SizeConfig.blockSizeHorizontal! * 0.5, 0,
        SizeConfig.blockSizeHorizontal! * 0.5, 0),
    indicatorWeight: 0.0001,
    tabs: [
      Container(
        // color: Colors.red,
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(
              color: Colors.white,
            ),
            borderRadius: const BorderRadius.all(Radius.circular(2))),
        padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal! * 2),
        height: SizeConfig.blockSizeVertical! * 5,
        width: SizeConfig.blockSizeHorizontal! * 35,
        child: Center(
            child: Text(
          "Squad",
          style: TextStyle(
              fontSize: SizeConfig.blockSizeHorizontal! * 4,
              color: Colors.black),
        )),
      ),
      Container(
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(
              color: Colors.white,
            ),
            borderRadius: const BorderRadius.all(Radius.circular(2))),
        padding: EdgeInsets.all(SizeConfig.blockSizeHorizontal! * 2),
        height: SizeConfig.blockSizeVertical! * 5,
        width: SizeConfig.blockSizeHorizontal! * 35,
        child: Center(
            child: Text(
          "Stats",
          style: TextStyle(
              fontSize: SizeConfig.blockSizeHorizontal! * 4,
              color: Colors.black),
        )),
      ),
    ],
  );
}

Widget appBarTitle() {
  return Align(
    alignment: Alignment.topLeft,
    child: Container(
      padding: EdgeInsets.fromLTRB(0, SizeConfig.blockSizeHorizontal! * 2, 0,
          SizeConfig.blockSizeHorizontal! * 2),
      height: SizeConfig.blockSizeHorizontal! * 16,
      width: SizeConfig.blockSizeHorizontal! * 20,
      child: const Flexible(
        child: Text(
          "Premier League",
          maxLines: 2,
          softWrap: false,
          textAlign: TextAlign.start,
        ),
      ),
    ),
  );
}

// ignore: must_be_immutable
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Flutter Demo',
      home: MyHomePage(title: 'Premier League'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

// List _team = [];
// var _stats;
int pageIndex = 1;
Widget _body = const CircularProgressIndicator();
var statsData = List.generate(51, (index) => "{}");
var squadData = List.generate(51, (index) => "{}");
String content = "";
TeamResponse? _team;
StatsResponse? _stats;

class _MyHomePageState extends State<MyHomePage> {
  @override
  void initState() {
    super.initState();
    setInitialValues();
    // pageIndex = UserSimplePreferences.getCurrentState() ?? 1;
    // readJson();
  }

  void setInitialValues() async {
    pageIndex = await readPageIndex();
    await writeSquad();
    await writeStats();
    content = await readSquad();
    var dataSquad = await json.decode(content);
    // print(dataSquad);
    // print(dataSquad["response"][0]);
    content = await readStats();
    var dataStats = await json.decode(content);
    TeamResponse t;
    StatsResponse s;
    // print(dataSquad["response"]);
    t = TeamResponse.fromJson(dataSquad["response"][0]);
    // print(dataStats["response"]);
    s = StatsResponse.fromJson(dataStats["response"]);
    // print(s);
    // print(t.team.name);
    setState(() {
      _team = t;
      _stats = s;
      // _team = dataSquad["response"];
      // _stats = dataStats["response"];
    });
  }

  // void getTeam() async {
  // final file = await writeTeam();
  // final String content = await readTeam();
  // var data = await json.decode(content);
  // data = data[2];
  // print(data["response"]);
  // var d = await json.decode(data);
  // print("hello $d");
  // }

  // Future<void> _setCurrentState() async {
  // await UserSimplePreferences.setCurrentState(pageIndex);
  // print(pageIndex);
  // }

  void _increment() async {
    pageIndex++;
    if (pageIndex > 50) {
      pageIndex = 1;
    }
    await writePageIndex();
    await writeSquad();
    await writeStats();
    // _setCurrentState();
    await setValues();
    // readJson();
  }

  void _decrement() async {
    pageIndex--;
    if (pageIndex == 0) {
      pageIndex = 50;
    }
    await writePageIndex();
    await writeSquad();
    await writeStats();
    await setValues();
    // _setCurrentState();
    // readJson();
  }

  // Future<void> _setData() async {
  //   await UserSimplePreferences.setTeam(pageIndex);
  //   await UserSimplePreferences.setStats(pageIndex);
  // }

  Future<void> setValues() async {
    setState(() {
      _team = null;
      _stats = null;
    });
    content = await readSquad();
    var dataSquad = await json.decode(content);
    content = await readStats();
    var dataStats = await json.decode(content);
    // setState(() {
    //   _team = dataSquad["response"];
    //   _stats = dataStats["response"];
    // });
    TeamResponse t;
    StatsResponse s;
    t = TeamResponse.fromJson(dataSquad["response"][0]);
    s = StatsResponse.fromJson(dataStats["response"]);
    // print(t.team.name);
    setState(() {
      _team = t;
      _stats = s;
      // _team = dataSquad["response"];
      // _stats = dataStats["response"];
    });
  }

  // Future<void> readJson() async {
  //   setState(() {
  //     _team = [];
  //     _stats = null;
  //   });
  //   await _setData();
  //   var team = UserSimplePreferences.getTeam(pageIndex) ?? "";
  //   var stats = UserSimplePreferences.getStats(pageIndex) ?? "";
  //   // print("$pageIndex");
  //   var dataTeam = await json.decode(team);
  //   var dataStats = await json.decode(stats);
  //   print("hello $dataTeam");
  //   setState(() {
  //     _stats = dataStats["response"];
  //     _team = dataTeam["response"];
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(255, 56, 0, 60),
        leading: Tab(
          icon: Image.asset(
            "assets/images/logo.jpeg",
            height: SizeConfig.blockSizeHorizontal! * 15,
            width: SizeConfig.blockSizeHorizontal! * 15,
          ),
        ),
        title: appBarTitle(),
      ),
      body: (_team == null || _stats == null)
          ? _body
          : Column(
              children: <Widget>[
                GestureDetector(
                  onHorizontalDragEnd: (details) {
                    if (details.primaryVelocity! < 0) {
                      _increment();
                    } else {
                      _decrement();
                    }
                  },
                  child: Container(
                    width: SizeConfig.blockSizeHorizontal! * 100,
                    height: SizeConfig.blockSizeVertical! * 30,
                    color: Colors.red,
                    child: Column(
                      children: <Widget>[
                        Container(
                            width: SizeConfig.blockSizeHorizontal! * 40,
                            height: SizeConfig.blockSizeHorizontal! * 40,
                            margin: EdgeInsets.fromLTRB(
                                0,
                                SizeConfig.blockSizeVertical! * 2,
                                0,
                                SizeConfig.blockSizeVertical! * 1),
                            padding: EdgeInsets.all(
                                SizeConfig.blockSizeVertical! * 2),
                            decoration: const BoxDecoration(
                              color: Colors.white,
                              shape: BoxShape.circle,
                            ),
                            child: Image.network(
                              _team!.team.pic,
                              // height: SizeConfig.blockSizeHorizontal! * 10,
                              // width: SizeConfig.blockSizeHorizontal! * 10,
                            )),
                        Text(
                          _team!.team.name,
                          style: TextStyle(
                              fontSize: SizeConfig.blockSizeHorizontal! * 7,
                              color: Colors.white),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  width: SizeConfig.blockSizeHorizontal! * 100,
                  color: Colors.red,
                  child: DefaultTabController(
                    length: 2,
                    child: Column(children: <Widget>[
                      Container(
                        margin: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                        child: tabBar(),
                      ),
                      Container(
                        color: Colors.white,
                        height: SizeConfig.blockSizeVertical! * 50,
                        child: TabBarView(
                          children: [
                            squad(),
                            stats(),
                          ],
                        ),
                      ),
                    ]),
                  ),
                ),
              ],
            ),
    ); // This trailing comma makes auto-formatting nicer for build methods.
    // This method is rerun every time setState is called
  }
}

Future<String> get _localPath async {
  final directory = await getApplicationDocumentsDirectory();
  return directory.path;
}

Future<File> get _localPageIndex async {
  final path = await _localPath;
  return File('$path/PageIndex.txt');
}

Future<File> get _localStatsFile async {
  final path = await _localPath;
  return File('$path/StatsData.txt');
}

Future<File> get _localSquadFile async {
  final path = await _localPath;
  return File('$path/SquadData.txt');
}

Future<void> writeSquad() async {
  if (squadData[pageIndex] != "{}") return;
  final file = await _localSquadFile;
  String apiUrl =
      'https://v3.football.api-sports.io/players/squads?team=$pageIndex';
  HttpClient client = HttpClient();
  client.autoUncompress = true;
  final HttpClientRequest request = await client.getUrl(Uri.parse(apiUrl));
  request.headers
      .set(HttpHeaders.contentTypeHeader, "application/json;charset=UTF-8");
  request.headers.set("X-Rapidapi-Key", "db6802318ba45cb867485a165d382e1e");
  final HttpClientResponse response = await request.close();
  final String data = await response.transform(utf8.decoder).join();
  squadData[pageIndex] = data;
  file.writeAsString(squadData.toString());
}

Future<String> readSquad() async {
  try {
    final file = await _localSquadFile;
    if (!file.existsSync()) {
      String path = await _localPath;
      await File('$path/SquadData.txt').create(recursive: true);
    }
    String contents = await file.readAsString();
    if (contents == "") {
      await writeSquad();
      contents = await file.readAsString();
      var tempList = await json.decode(contents);
      var tempData = tempList[pageIndex];
      return json.encode(tempData);
    }
    var tempList = await json.decode(contents);
    var tempData = tempList[pageIndex];
    if (tempData == "") {
      await writeSquad();
      contents = await file.readAsString();
      tempList = await json.decode(contents);
      tempData = tempList[pageIndex];
    }
    return json.encode(tempData);
  } catch (e) {
    print(e);
    return "error";
  }
}

Future<void> writeStats() async {
  if (statsData[pageIndex] != "{}") return;
  final file = await _localStatsFile;
  String apiUrl =
      'https://v3.football.api-sports.io/teams/statistics?league=39&season=2019&team=$pageIndex';
  HttpClient client = HttpClient();
  client.autoUncompress = true;
  final HttpClientRequest request = await client.getUrl(Uri.parse(apiUrl));
  request.headers
      .set(HttpHeaders.contentTypeHeader, "application/json;charset=UTF-8");
  request.headers.set("X-Rapidapi-Key", "db6802318ba45cb867485a165d382e1e");
  final HttpClientResponse response = await request.close();
  final String data = await response.transform(utf8.decoder).join();
  statsData[pageIndex] = data;
  file.writeAsString(statsData.toString());
}

Future<String> readStats() async {
  try {
    final file = await _localStatsFile;
    if (!file.existsSync()) {
      String path = await _localPath;
      await File('$path/StatsData.txt').create(recursive: true);
    }
    String contents = await file.readAsString();
    if (contents == "") {
      await writeStats();
      contents = await file.readAsString();
      var tempList = await json.decode(contents);
      var tempData = tempList[pageIndex];
      return json.encode(tempData);
    }
    var tempList = await json.decode(contents);
    var tempData = tempList[pageIndex];
    if (tempData == "") {
      await writeStats();
      contents = await file.readAsString();
      tempList = await json.decode(contents);
      tempData = tempList[pageIndex];
    }
    return json.encode(tempData);
  } catch (e) {
    print(e);
    return "error";
  }
}

Future<void> writePageIndex() async {
  final file = await _localPageIndex;
  file.writeAsString(pageIndex.toString());
}

Future<int> readPageIndex() async {
  final file = await _localPageIndex;
  if (!file.existsSync()) {
    String path = await _localPath;
    await File('$path/PageIndex.txt').create(recursive: true);
  }
  final contents = await file.readAsString();
  if (contents == "") {
    await writePageIndex();
    return 1;
  }
  return int.parse(contents);
}
