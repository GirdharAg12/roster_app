import 'package:json_annotation/json_annotation.dart';
import 'team.dart';
import 'player.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'teamResponse.g.dart';

@JsonSerializable(explicitToJson: true)
class TeamResponse {
  TeamResponse(this.team, this.players);

  @JsonKey(name: 'team')
  Team team;

  @JsonKey(name: 'players')
  List<Player> players;

  factory TeamResponse.fromJson(Map<String, dynamic> json) =>
      _$TeamResponseFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$TeamResponseToJson(this);
}
