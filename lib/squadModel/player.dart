import 'package:json_annotation/json_annotation.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'player.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.

@JsonSerializable(explicitToJson: true)
class Player {
  Player(this.name, this.designation, this.number, this.pic);

  @JsonKey(name: 'name')
  String name;

  @JsonKey(name: 'position')
  String designation;

  @JsonKey(name: 'number', defaultValue: 0)
  int number;

  @JsonKey(name: 'photo')
  String pic;

  factory Player.fromJson(Map<String, dynamic> json) => _$PlayerFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$PlayerToJson(this);
}
