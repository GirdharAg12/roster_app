// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'teamResponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TeamResponse _$TeamResponseFromJson(Map<String, dynamic> json) => TeamResponse(
      Team.fromJson(json['team'] as Map<String, dynamic>),
      (json['players'] as List<dynamic>)
          .map((e) => Player.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$TeamResponseToJson(TeamResponse instance) =>
    <String, dynamic>{
      'team': instance.team.toJson(),
      'players': instance.players.map((e) => e.toJson()).toList(),
    };
