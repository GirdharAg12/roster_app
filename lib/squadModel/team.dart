
import 'package:json_annotation/json_annotation.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'team.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.

@JsonSerializable(explicitToJson: true)
class Team {
  Team(this.name, this.pic);

  @JsonKey(name: 'name')
  String name;

  @JsonKey(name: 'logo')
  String pic;

  factory Team.fromJson(Map<String, dynamic> json) => _$TeamFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$TeamToJson(this);
}
